import React, { useMemo, useRef } from 'react';
import { Marker } from 'react-leaflet';
import * as L from 'leaflet';
import { createUseStyles } from 'react-jss'

function DraggableMarker(props) {
    const size = 40;

    const useStyles = createUseStyles({
        myMarker: {
            fontSize: `${size}px`
        }
    })

    const classes = useStyles();

    const icon = new L.divIcon({
        iconSize: [size, size],
        iconAnchor: [size / 2, size / 2],
        html: `${props.marker}`,
        className: `${classes.myMarker}`
    });

    const markerRef = useRef(null)
    const eventHandlers = useMemo(
        () => ({
            dragend() {
                const marker = markerRef.current
                if (marker != null) {
                    props.positionChangeCallback(marker.getLatLng());
                }
            },
        }),
        [props]
    )

    return (
        <Marker
            draggable
            eventHandlers={eventHandlers}
            position={props.position}
            ref={markerRef}
            icon={icon}
        />
    )
}

export default DraggableMarker;