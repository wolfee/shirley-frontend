import React, { useState } from 'react';
import { MapContainer, TileLayer, Polyline } from 'react-leaflet';
import './App.css';
import DraggableMarker from './DraggableMarker';
import styled from "styled-components";
import decodePolyline from "decode-google-map-polyline";

const AbsoulteButton = styled.button`
  ${props => props.top && `top: ${props.top}px;`}
  ${props => props.bottom && `bottom: ${props.bottom}px;`}
  ${props => props.left && `left: ${props.left}px;`}
  ${props => props.right && `right: ${props.right}px;`}
  position: absolute;
  z-index: 400;
  background-color: white;
  color: rgb(60, 64, 67);
  border-radius: 1rem;
  line-height: 1.5rem;
  font-size: 0.875rem;
  font-weight: 500;
  padding: 0rem 0.875rem;
  box-shadow: 0px 2px 2px lightgray;
  transition: ease background-color 100ms;
  border: 0;
  height: 32px;
  box-shadow: rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px;
  &:hover {
    background-color: rgb(247, 247, 247);
  }
  &:active {
    background-color: rgb(200, 200, 200);
  }
`;

function App() {
  const [waypoints, setWaypoints] = useState([]);
  const [center, setCenter] = useState({ lat: 41.390205, lng: 2.154007 });
  const [path, setPath] = useState(null);

  class waypoint {
    constructor(position, icon, id) {
      this.position = position;
      this.icon = icon;
      this.id = id;
    }
  };

  if (waypoints.length === 0) {
    const wps = [];
    wps.push(new waypoint({ lat: 41.374601, lng: 2.1316655 }, '🚗', 0));
    wps.push(new waypoint({ lat: 41.3558106, lng: 2.138902 }, '🏁', 1));

    let sumLat = 0;
    let sumLng = 0;
    for (let wp of wps) {
      sumLat += wp.position.lat;
      sumLng += wp.position.lng;
    }
    sumLat /= wps.length;
    sumLng /= wps.length;

    setWaypoints(wps);

    setCenter({ lat: sumLat, lng: sumLng });
  }

  const route = async () => {
    const requestData = {
      id: 1,
      jsonrpc: "2.0",
      method: "plan_route",
      params: {
        waypoints: []
      }
    };

    for (const wp of waypoints) {
      requestData.params.waypoints.push({
        latitude: wp.position.lat,
        longitude: wp.position.lng
      })
    }

    if (path) {
      setPath(null);
    }

    const request = await fetch('http://localhost:8080', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(requestData)
    });

    if (request.ok) {
      const response = await request.json();
      const polyline = decodePolyline(response.result.polyline);

      const tmpPath = [];
      for (const p of polyline) {
        tmpPath.push([
          p.lat,
          p.lng
        ]);
      }

      setPath(tmpPath);

    } else {
      console.log("something is wrong");
    }
  };

  return (
    <div className="App">
      <MapContainer center={center} zoom={15} zoomControl={false}>
        <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution="&copy; <a href=&quot;https://www.openstreetmap.org/copyright&quot;>OpenStreetMap</a> contributors" />
        {
          waypoints.map(element => { return <DraggableMarker key={element.id} position={element.position} positionChangeCallback={(param) => { element.position = param; }} marker={element.icon} /> })
        }
        {path && <Polyline pathOptions={{ color: 'red' }} positions={path} />}
      </MapContainer>
      <AbsoulteButton top={20} left={20} onClick={route}>
        Route
      </AbsoulteButton>
    </div>
  );
}

export default App;
